import { AppBar, Avatar, Box, Toolbar, Typography } from "@mui/material";
import React from "react";
import {
  ArrowForwardOutlined,
  FacebookOutlined,
  Instagram,
  LinkedIn,
  Person,
  PhoneIphone,
  Subscriptions,
} from "@mui/icons-material";
import Group36 from "../assets/group-36.png";
import "./index.css";
import { Link } from "react-router-dom";

const MainLayout = ({ children }) => {
  return (
    <div className="app-container">
      <AppBar
        sx={{
          backgroundColor: "#fff!important",
          boxShadow: "none!important",
          color: "#707070!important",
        }}
        position="static"
      >
        <Toolbar disableGutters>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              flex: 1,
              paddingTop: "58px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-start",
                flex: 0.5,
                alignItems: "center",
                paddingLeft: "93px",
              }}
            >
              <Avatar
                alt="Soumyadeep Pal"
                src={Group36}
                sx={{ width: 73, height: 73 }}
              />
              <Typography sx={{ fontSize: "37px" }}>
                <Link to="/">Soumyadeep Pal</Link>
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                flex: 0.5,
                alignItems: "center",
                paddingRight: "73px",
              }}
            >
              <Typography>
                <Link to="/profile">
                  <Person fontSize="medium" /> Profile
                </Link>
              </Typography>
              <Typography>
                <Link to="/ui-ux">
                  <PhoneIphone fontSize="medium" />
                  UI/UX
                </Link>
              </Typography>
              <Typography>
                <Link to="/social-media">
                  <Subscriptions fontSize="medium" />
                  Social Media Posts
                </Link>
              </Typography>
              <Typography>
                <Link to="manipulations">
                  <Subscriptions fontSize="medium" />
                  Manipulations
                </Link>
              </Typography>
              {/* <Typography>
              Get In Touch
              <ArrowForwardOutlined fontSize="medium" />
            </Typography> */}
            </Box>
          </Box>
        </Toolbar>
      </AppBar>

      {children}
      <AppBar
        sx={{
          backgroundColor: "#434343!important",
          borderColor: "#707070",
          boxShadow: "none",
          justifyContent: "flex-end",
          alignItems: "flex-end",
          top: "auto",
          bottom: 0
        }}
        position="fixed"
        color="primary"
      >
        <Toolbar>
          <LinkedIn />
          <FacebookOutlined />
          <Instagram />
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default MainLayout;
