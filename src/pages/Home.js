import { Box, Button, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import { Link } from "react-router-dom";
import bgImage from "../assets/a-2729794.png";

const useStyles = makeStyles(() => ({
  homepage__bg: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundImage: `url(${bgImage})`,
    backgroundPosition: "bottom right",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    height: '100%'
  },
  header_text: {
    fontSize: "92px",
    textAlign: "center",
    width: "765px",
    color: "#707070",
  },
  smw_btn: {
    width: "215px",
    height: "71px",
    padding: "17px 29px",
    borderRadius: "7px",
    backgroundColor: "#434343",
    marginTop: "77px",
    "&:hover": {
      backgroundColor: "#434343",
    },
  },
}));

const Home = () => {
  const styles = useStyles();
  return (
    <Box className={styles.homepage__bg}>
      <Typography className={styles.header_text}>
        Making Designs and User Interface Simpler
      </Typography>
      <Link to="/home-detail">
        <Button className={styles.smw_btn} variant="contained">
          See My Work
        </Button>
      </Link>
    </Box>
  );
};

export default Home;
