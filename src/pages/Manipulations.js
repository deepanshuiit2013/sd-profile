import React, { useState } from 'react';
import {makeStyles} from '@mui/styles';
import img1 from "../assets/ManiHeader.webp"
import HorizontalAccordian from '../components/HorizontalAccordian';
import { Box, Drawer, Typography } from '@mui/material';

const DATA = [
  {
    name: 'Windowed',
    title: 'Windowed',
    urls : ['https://drive.google.com/uc?export=view&id=1ghi3i_nVC_gtByGTLC6LNhBRhxxHXUVc', 'https://drive.google.com/uc?export=view&id=1OnJbbTqsoUXw-0Dj_Mb1jp-lqc_a0RKY'],
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   },
  
   {
    name: 'The Distance',
    urls : ['https://drive.google.com/uc?export=view&id=1F23rRSvxfyHZODDlcfnAIm-hyTHc6A9o', 'https://drive.google.com/uc?export=view&id=1Xnmdnp3OWzTXDhhRrlJx4UVD9UOBO7fq'],
    title: 'The Distance',
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   },
  
   {
    name: 'Used To it',
    urls : ['https://drive.google.com/uc?export=view&id=1Wk4lMZa52Cof18zsiTxq5XO_cbwLhIFF', 'https://drive.google.com/uc?export=view&id=1A9qKTSGsvZeEfH7Iz6Ag1jHgtfLBzE29'],
    title: 'Used To it',
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   },
  
   {
    name: 'The Eyes',
    urls : ['https://drive.google.com/uc?export=view&id=1umrHHiKDvN8TXafaReXLJe4nj2fuoaPr', 'https://drive.google.com/uc?export=view&id=1DgtD3iWl0uhLFccj8D_wdLrga5Z4mE2c'],
    title: 'The Eyes',
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   },
  
   {
    name: 'MUSIKKRIEG',
    urls : ['https://drive.google.com/uc?export=view&id=1v1IdxsRV20MNRqlzmsp1Kkq8IubYNjuZ', 'https://drive.google.com/uc?export=view&id=11eH0UdXWnAbFwBfavvjo2YU4FvfT5iDu'],
    title: '',
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   },
  
   {
    name: 'Terrestrial',
    urls : ['https://drive.google.com/uc?export=view&id=1OKMeZxJENSvvJtzmkrbO0DifBBWCfG1R', 'https://drive.google.com/uc?export=view&id=1XECdWKiebiZ-p9ib3Aq_JI-u79gBVxKR'],
    title: 'Terrestrial',
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   },
  
   {
    name: 'Bare Love',
    urls : ['https://drive.google.com/uc?export=view&id=1Tsy3s_nboug6Np_6_fqP8gpxRsn-FYLs', 'https://drive.google.com/uc?export=view&id=1gV9ssbesKKos9RZeia1JgwToXAZ8zgYO'],
    title: 'Bare Love',
    description: 'As we try to build successful citizens, critical study of pictures may be a useful vehicle for helping children critically examine a range of sources. Although it is simple to draw ethical lines against images like this one that are clear frauds, there is growing discussion in the media over how much image modification is permissible.',
   }
 ]

const useStyles = makeStyles(() => ({
  pageContainer: {
    height: '100%',
    textAlign: 'center'
  },
  bandTitle: {
    height: '73px',
    backgroundColor: '#565656',
    fontSize: '26px',
    letterSpacing: '21.68px',
    color: '#fff',
    lineHeight: '73px'
  },
  drawerPaper: {
    height: '700px',
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  }
}))

const Manipulations = () => {
  const styles = useStyles();

  const [isOpen, setIsOpen] = useState(false)
  const [selectedItem, setSelectedItem] = useState(undefined)
  const [selectedImageUrl, setSelectedImageUrl] = useState(undefined)

  const onItemClick = (item) => {
    setSelectedImageUrl(undefined)
    setSelectedItem(item)
    setIsOpen(true)
  }

  return ( <div className={styles.pageContainer}>
    <img src={img1} alt="smHeader" />

    <p className={styles.bandTitle}>MY DESIGNS</p>

    <HorizontalAccordian onItemClick={onItemClick} items={DATA}/>

    <Drawer anchor="bottom" open={isOpen} onClose={() => setIsOpen(false)} PaperProps={{sx:{borderRadius: "50px 50px 0 0", padding: '67px 198px 57px 236px'}}}>
      <Box className={styles.drawerPaper} >
        <Box>
          <Typography sx={{fontSize: '82px'}} variant="h1" align="left">{selectedItem?.title}</Typography>
          <Typography sx={{fontSize: '28px'}} variant="body1">{selectedItem?.description}</Typography>
        </Box>
        <Box>
          {selectedItem && <img src={selectedImageUrl || selectedItem?.urls[0]} height="606" width="606" alt="headerImage"/>}
          <Box sx={{display: 'flex', justifyContent:"center"}}>
            {selectedItem?.urls?.map((url, index) =>(<img onClick={() => setSelectedImageUrl(url)} height="140" width="140" src={url} alt={index} key={index} />))}
          </Box>
        </Box>
      </Box>
    </Drawer>

  </div> );
}
 
export default Manipulations;