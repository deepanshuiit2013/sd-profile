import { Grid, a } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import im1 from "../assets/HmDet1.png";
import im2 from "../assets/HmDet2.png";
import im3 from "../assets/HmDet3.png";
const useStyles = makeStyles(() => ({
  gridContainer: {
    height: "100%",
  },
  gridPositions: {
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
  },
}));

const HomeDetail = () => {
  const styles = useStyles();

  return (
    <div className={styles.gridContainer}>
      <Grid className={styles.gridPositions} container spacing={2}>
        <Grid item>
          <a href="/ui-ux">
            <img alt="det1" src={im3} />
          </a>
        </Grid>
        <Grid item>
          <a href="/social-media">
            <img alt="det1" src={im2} border="0" />
          </a>
        </Grid>
        <Grid item>
          <a href="/manipulations">
            <img alt="det1" src={im1} />
          </a>
        </Grid>
      </Grid>
    </div>
  );
};

export default HomeDetail;
