import React, { useState } from "react";
import { makeStyles } from "@mui/styles";
import img1 from "../assets/ui-uxHeader.webp";
import HorizontalAccordian from "../components/HorizontalAccordian";
import { Box, Drawer, Typography } from "@mui/material";

const DATA = [
  {
    title: "Navidu Mobile App",
    subtitle: "UI/UX Designs",
    urls: [
      "https://drive.google.com/uc?export=view&id=10NLSavlEhSkpcGU9KJTX1VZqNahaeC5P",
      "https://drive.google.com/uc?export=view&id=19kCNccPWXAn4PKbEjLW7eP_cIsOsWNdu",
      "https://drive.google.com/uc?export=view&id=1AfXJejMhDiGtt5jLsEZnpKSFJ-F4Zdpq",
    ],
    description:
      "Designed and delivered, useful and elegant, multi-platform Application and website designs for the incognito mode startup that aims towards constituting Real estate as a service over a platform.",
  },

  {
    title: "Meme-T",
    subtitle: "UI/UX Designs",
    urls: [
      "https://drive.google.com/uc?export=view&id=1WTWSu1DE6cne9-jekpYDwAIpKs6Z_9V3",
      "https://drive.google.com/uc?export=view&id=1EunVA_KIjiA58P4lraM9CRgLVmNx642R",
      "https://drive.google.com/uc?export=view&id=1BYinLM9TX3Dpj7oAhCxV3LDdlD_oAGTf",
    ],
    description:
      "Collaborated with the Founder and engineers to design and develop a multi-platform website online e-commerce that targets promoting regional languages through fashion technology along with the promise to sellers to sell better.",
  },
];

const useStyles = makeStyles(() => ({
  pageContainer: {
    height: "100%",
    textAlign: "center",
  },
  bandTitle: {
    height: "73px",
    backgroundColor: "#565656",
    fontSize: "26px",
    letterSpacing: "21.68px",
    color: "#fff",
    lineHeight: "73px",
  },
  drawerPaper: {
    height: "700px",
    width: "100%",
  },
}));

const UI_UX = () => {
  const styles = useStyles();

  const [isOpen, setIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(undefined);

  const onItemClick = (item) => {
    setSelectedItem(item);
    setIsOpen(true);
  };

  return (
    <div className={styles.pageContainer}>
      <img src={img1} alt="smHeader" />

      <p className={styles.bandTitle}>MY DESIGNS</p>

      <HorizontalAccordian onItemClick={onItemClick} items={DATA} />

      <Drawer
        anchor="bottom"
        open={isOpen}
        onClose={() => setIsOpen(false)}
        PaperProps={{
          sx: {
            borderRadius: "50px 50px 0 0",
          },
        }}
      >
        <Box className={styles.drawerPaper}>
          <Box sx={{ display: "flex", padding: "30px 99px 0 196px", alignItems: 'center' }}>
            <Box>
              <Typography sx={{ fontSize: "82px" }} variant="h1" align="left">
                {selectedItem?.title}
              </Typography>
              <Typography
                sx={{
                  width: "700px",
                  fontFamily: "SegoeUI",
                  fontSize: "28px",
                  fontWeight: "normal",
                  fontStretch: "normal",
                  fontStyle: "normal",
                  lineHeight: "1.32",
                  letterSpacing: "normal",
                  textAlign: "left",
                  color: "#000",
                }}
              >{selectedItem?.subtitle}</Typography>
              <Typography sx={{ fontSize: "28px" }} variant="body1">
                {selectedItem?.description}
              </Typography>
            </Box>
            <Box>
              {selectedItem && (
                <img
                  src={selectedItem?.urls[0]}
                  height="606"
                  width="606"
                  alt="headerImage"
                />
              )}
            </Box>
          </Box>

          <Typography
            sx={{
              idth: "769px",
              height: "65px",
              fontFamily: "SegoeUI",
              fontSize: "49px",
              fontWeight: "600",
              fontStretch: "normal",
              fontStyle: "normal",
              lineHeight: "1.33",
              letterSpacing: "normal",
              textAlign: "center",
              color: "#707070",
              marginTop: "106px"
            }}
          >
            Design Wireframes and Work Flow
          </Typography>
          {selectedItem && (
            <img
              src={selectedItem?.urls[1]}
              height="620"
              width="1775"
              alt="headerImage"
            />
          )}
          <Typography
            sx={{
              idth: "769px",
              height: "65px",
              fontFamily: "SegoeUI",
              fontSize: "49px",
              fontWeight: "600",
              fontStretch: "normal",
              fontStyle: "normal",
              lineHeight: "1.33",
              letterSpacing: "normal",
              textAlign: "center",
              color: "#707070",
              marginTop: "130px"
            }}
          >
            Design Wireframes and Work Flow
          </Typography>
          {selectedItem && (
            <img
              src={selectedItem?.urls[2]}
              height="620"
              width="1775"
              alt="headerImage"
            />
          )}
        </Box>
      </Drawer>
    </div>
  );
};

export default UI_UX;
