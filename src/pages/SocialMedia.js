import React, { useState } from 'react';
import {makeStyles} from '@mui/styles';
import img1 from "../assets/SMHeader.webp"
import HorizontalAccordian from '../components/HorizontalAccordian';
import { Box, Drawer, Typography } from '@mui/material';

const DATA = [
  {
    name: 'Kave',
    urls : ['https://drive.google.com/uc?export=view&id=15Syp0b_oX8ByFF2bT2-UMuMpSl7ShsON', 'https://drive.google.com/uc?export=view&id=1UEmhhbRxxj8N0RtCh-gipmgjjrpWcpkT'],
    title: 'Kave',
    description: 'Branding has always been a crucial aspect of company, but it may be more critical now than ever. Every day, customers are exposed to new companies thanks to social media. This is excellent for customers who have a lot of alternatives and can perform research to choose the best one, but it makes it difficult for companies.',
   },
 {
    name: 'Bounce',
    urls : ['https://drive.google.com/uc?export=view&id=1HK__lF84vEZ9-N-cr2Bmgh7lcg7aibs8', 'https://drive.google.com/uc?export=view&id=12f4Swc1y1wq3dEor8TSS-uvCxqVThvGU'],
    title: 'Bonuce',
    description: 'Branding has always been a crucial aspect of company, but it may be more critical now than ever. Every day, customers are exposed to new companies thanks to social media. This is excellent for customers who have a lot of alternatives and can perform research to choose the best one, but it makes it difficult for companies.',
   },
 {
    name: 'Navidu',
    urls : ['https://drive.google.com/uc?export=view&id=1nJQUlku2lk6C1KIxzyO0iAnqqX7f32O9', 'https://drive.google.com/uc?export=view&id=1TTNG_q7T6mGL-GvRBjyY3MDjfr1AIEBo'],
    title: 'Navidu',
    description: 'One of the primary benefits of developing a high-quality Coming Soon website is that it may aid in the creation of buzz before your service goes live. It has the potential to sow a significant seed in the minds of your target audience. With the addition of a countdown timer, you may even create a sense of urgency.',
   },
 {
    name: 'Navidu',
    urls : ['https://drive.google.com/uc?export=view&id=1bGHBWjFAu1SHtE7t9pZvH2loaZmxi_my', 'https://drive.google.com/uc?export=view&id=1MYUdmqabMyH5dqsIWuoD_5DYs8GwW3cD'],
    title: 'Navidu',
    description: 'One of the primary benefits of developing a high-quality Coming Soon website is that it may aid in the creation of buzz before your service goes live. It has the potential to sow a significant seed in the minds of your target audience. With the addition of a countdown timer, you may even create a sense of urgency.',
   },
 {
    name: 'Real Estate',
    urls : ['https://drive.google.com/uc?export=view&id=1lAR69QAF3EaknyoAJ7WqpB9K5rzl1uqq', 'https://drive.google.com/uc?export=view&id=1XdLKoMVIrl3_tGhlCrKa2wfJv4FwHjj2'],
    title: 'Real Estate',
    description: 'Customer engagement is the continual development of a relationship between a firm and a customer that extends long beyond the transaction. It is a deliberate, consistent method used by a corporation to deliver value at every client encounter, hence fostering loyalty.',
   }
 
]

const useStyles = makeStyles(() => ({
  pageContainer: {
    height: '100%',
    textAlign: 'center'
  },
  bandTitle: {
    height: '73px',
    backgroundColor: '#565656',
    fontSize: '26px',
    letterSpacing: '21.68px',
    color: '#fff',
    lineHeight: '73px'
  },
  drawerPaper: {
    height: '700px',
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  }
}))

const SocialMedia = () => {
  const styles = useStyles();

  const [isOpen, setIsOpen] = useState(false)
  const [selectedItem, setSelectedItem] = useState(undefined)
  const [selectedImageUrl, setSelectedImageUrl] = useState(undefined)

  const onItemClick = (item) => {
    setSelectedImageUrl(undefined)
    setSelectedItem(item)
    setIsOpen(true)
  }

  return ( <div className={styles.pageContainer}>
    <img src={img1} alt="smHeader" />

    <p className={styles.bandTitle}>MY DESIGNS</p>

    <HorizontalAccordian onItemClick={onItemClick} items={DATA}/>

    <Drawer anchor="bottom" open={isOpen} onClose={() => setIsOpen(false)} PaperProps={{sx:{borderRadius: "50px 50px 0 0", padding: '67px 198px 57px 236px'}}}>
      <Box className={styles.drawerPaper} >
        <Box>
          <Typography sx={{fontSize: '82px'}} variant="h1" align="left">{selectedItem?.title}</Typography>
          <Typography sx={{fontSize: '28px'}} variant="body1">{selectedItem?.description}</Typography>
        </Box>
        <Box>
          {selectedItem && <img src={selectedImageUrl || selectedItem?.urls[0]} height="606" width="606" alt="headerImage"/>}
          <Box sx={{display: 'flex', justifyContent:"center"}}>
            {selectedItem?.urls?.map((url, index) =>(<img onClick={() => setSelectedImageUrl(url)} height="140" width="140" src={url} alt={index} key={index} />))}
          </Box>
        </Box>
      </Box>
    </Drawer>

  </div> );
}
 
export default SocialMedia;