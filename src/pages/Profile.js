import { Avatar, Box, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import img1 from "../assets/skilTools.png";
import coverImg from "../assets/profileCoverImage.png";
import avatarImg from "../assets/profileAvatarImage.png";


const useStyles = makeStyles(() => ({
  bannerImage: {
    height: '340px',
    backgroundImage: `url(${coverImg})`
  },
  bannerImageOverLay: {
    display: 'flex',
    alignItems: 'flex-end',
    paddingTop: '205px',
    paddingLeft: '109px'
  }
}))

const Profile = () => {
  const styles = useStyles();
  return (
    <>
      <div className={styles.bannerImage}>
        <div className={styles.bannerImageOverLay}>
          <Avatar 
            src={avatarImg}
            sx={{ width: 248, height: 248 }}
          />
          <Typography
          sx={{
            // width: "643px",
            // height: "259px",
            fontFamily: "SegoeUI",
            fontSize: "54px",
            color: "#434343",
          }}
        >
          Soumyadeep Pal<br/>
          <Typography
          sx={{
            // width: "643px",
            // height: "259px",
            fontFamily: "SegoeUI",
            fontSize: "31px",
            color: "#434343",
          }}
        >
          jeetpal.125@gmail.com
        </Typography>
        </Typography>
        
        </div>
      </div>
      <Box sx={{ display: "flex", justifyContent: "space-around", paddingTop: '103px' }}>
        <Typography
          sx={{
            width: "643px",
            height: "259px",
            fontFamily: "SegoeUI",
            fontSize: "28px",
            color: "#434343",
          }}
        >
          Hi, <br />I am a motivated and dedicated graphics and UI/UX designer
          with special interest in creating meaningful and responsive
          interaction designs, motion designs, logo animation, 3D modeling,
          UI/UX research, and keen interest in converting exceptional ideas into
          tangible assets.
        </Typography>
        <img src={img1} alt="skillImage" />
      </Box>
    </>
  );
};

export default Profile;
