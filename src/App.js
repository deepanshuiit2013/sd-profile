import "./App.css";
import MainLayout from "./layout";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import UI_UX from "./pages/UI-UX";
import SocialMedia from "./pages/SocialMedia";
import HomeDetail from "./pages/HomeDetail";
import Manipulations from "./pages/Manipulations";
import Profile from "./pages/Profile";

function App() {
  return (
    <BrowserRouter>
      <MainLayout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home-detail" element={<HomeDetail />} />
          <Route path="/ui-ux" element={<UI_UX />} />
          <Route path="/social-media" element={<SocialMedia />}  />
          <Route path="/manipulations" element={<Manipulations />}  />
          <Route path="/get-in-touch" element={<SocialMedia />}  />
          <Route path="/profile" element={<Profile />}  />
        </Routes>
      </MainLayout>
    </BrowserRouter>
  );
}

export default App;
