import React from 'react';
import './index.css'
/**
 * {
 *  name:
 *  urls : ['', ''],
 *  title: ''
 *  description: ''
 * }
 * 
 */

const HorizontalAccordian = ({items, onItemClick}) => {
  return ( <div className="horizontal-accordian__container">
   {items.map((item, index) => (<img onClick={() => onItemClick(item)} src={item.urls[0]} alt="item name" key={index} />))}
  </div> );
}
 
export default HorizontalAccordian;